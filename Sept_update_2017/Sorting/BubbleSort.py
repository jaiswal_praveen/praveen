# Python program for implementation of Bubble Sort
from array import array
def bubbleSort(arr):
	n = len(arr)

	# Traverse through all array elements
	for i in range(n):

		# Last i elements are already in place
		for j in range(0, n-i-1):

			# traverse the array from 0 to n-i-1
			# Swap if the element found is greater
			# than the next element
			if arr[j] > arr[j+1] :
				arr[j], arr[j+1] = arr[j+1], arr[j]

# Program to test 
n = int(input("Enter length of array to be sorted:"))
arr=[]
for i in range (n):
    x=int(input("enter no. \n"))
    arr.insert(i,x)
    i+=1

bubbleSort(arr)

print ("Sorted array is:")
for i in range(len(arr)):
	print ("%d" %arr[i]),


