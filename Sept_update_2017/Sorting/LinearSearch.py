# If you want to implement Linear Search in python

# Linearly search x in arr[]
# If x is present then return its location
# else return -1

def LinearSearch(arr, x):
    for i in range(len(arr)):

        if arr[i] == x:
            return i

    return -1

# Program to test above function
n = int(input("Enter length of array:"))
arr=[]
for i in range (n):
    x=int(input("enter no.:"))
    arr.insert(i,x)
    i+=1

item = int(input("Enter item to be searched:"))
itemIndex = LinearSearch(arr, item)
if itemIndex != -1:
	print ("Item is found at index:", itemIndex)
else:
	print ("Item is not present in array")
